﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject ballS;
	[SerializeField] float randomF = 0.5f;
	Rigidbody2D myRigidBody;
	Vector2 myVek;

	private void Start()
	{
		float x = UnityEngine.Random.Range(0, randomF);
		float y = UnityEngine.Random.Range(0, randomF);
		myVek = new Vector2(x, y);
	}

	void Update()
    {
        
    }
	private void OnCollisionEnter2D(Collision2D collision)
	{
		Instantiate(ballS,transform.position,transform.rotation);
		myRigidBody.velocity = myVek;
		Destroy(gameObject);
	}

}
